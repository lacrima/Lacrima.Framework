﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lacrima.Framework.Infrastructure;
using Lacrima.Framework.Persistence;

namespace Lacrima.Framework.Test.Persistence
{
    [TestClass]
    public class RepositoryTest
    {
        public DBEntityEnum entity { get; set; }
        public IRepository<City> repository { get; set; }

        public RepositoryTest()
        {
            entity = DBEntityEnum.WORLD;
            MySQLManager MySQL = new MySQLManager(entity);
            repository = new MySQLRepository<City>(MySQL);
        }

        [TestMethod]
        public void TestMethod1()
        {
            var result = repository.QueryAll();
        }

        [TestMethod]
        public void TestMethod2()
        {
            var result = repository.QueryDefault();
        }

        [TestMethod]
        public void TestMethod3()
        {
            entity = DBEntityEnum.OpenTestDB;
            SQLServerManager SQLServer = new SQLServerManager(entity);
            IRepository<CityInfo> repo = new SQLServerRepository<CityInfo>(SQLServer);
            var ret = repo.QueryDefault();
        }

        [TestMethod]
        public void TestMethod4()
        {
            entity = DBEntityEnum.OpenTestDB;
            SQLServerManager SQLServer = new SQLServerManager(entity);
            IRepository<CityInfo> repo = new SQLServerRepository<CityInfo>(SQLServer);
            var ret = repo.QueryAll();
        }
    }

    public class City
    {
        public int Id { get; set; }
        public string  Name{ get; set; }
        public string CountryCode { get; set; }
        public string District { get; set; }
        public int Population { get; set; }
    }

    public class CityInfo
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int IsDel { get; set; }
        public DateTime CreateTime { get; set; }
        public int IsNorth { get; set; }
    }
}

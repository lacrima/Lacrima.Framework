﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lacrima.Framework.Persistence;
using System.Data;
using Lacrima.Framework.Infrastructure;
using MySql.Data.MySqlClient;

namespace Lacrima.Framework.Test.Persistence
{
    [TestClass]
    public class DbManagerTest
    {
        private IDbManager manager { get; set; }

        [TestMethod]
        public void TestMethod1()
        {
            string conn = "Database='mysql';Data Source='192.168.1.28';User Id='root';Password='';charset='utf8'";
            string query = "SELECT * FROM user";
            manager = new MySQLManager(conn);
            manager.Open();

            // 查询
            DataTable table = null;
            manager.ExecuteQuery(query,out table);

            MySqlDataReader reader = manager.ExecuteQuery(query) as MySqlDataReader;
            while(reader.Read())
            {
                if (reader.HasRows)
                {
                    string row = reader.GetString(0);
                }
            }
            var schema = reader.GetSchemaTable();

            manager.Close();
        }

        [TestMethod]
        public void TestMethod2()
        {
            string conn = "Database='bugtracker';Data Source='192.168.1.28';User Id='root';Password='';charset='utf8'";
            string query = "SELECT * FROM mantis_user_table";
            manager = new MySQLManager(conn);
            manager.Open();

            // 查询
            DataTable table = null;
            manager.ExecuteQuery(query, out table);

            string nonQuery = "UPDATE mantis_user_table SET REALNAME = 'allen'";
            manager.ExecuteNonQuery(nonQuery);

            manager.Close();
        }
    }
}

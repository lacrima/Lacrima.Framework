﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lacrima.Framework.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace Lacrima.Framework.Test.Infrastructure.Adapter
{
    [TestClass]
    public class DocumentGeneratorTest
    {
        protected LinqToXmlAdapter xmlAdapter { get; set; }
        protected List<BookInfo> bookList { get; set; }
        protected string directory { get; set; }
        protected string fileType { get; set; }
        protected string fileName { get; set; }

        public DocumentGeneratorTest()
        {
            xmlAdapter = new LinqToXmlAdapter();
            bookList = xmlAdapter.GetBookList();
            directory = @"..\Lacrima.Framework.Test\";
            fileName = Generator.UID;
            fileType = "xml";
        }

        [TestMethod]
        public void SaveTest()
        {
            bookList.Save(directory, fileName, fileType,SaveType.Add);

            bookList.First().Author = Generator.UID;

            bookList.Save(directory, fileName, fileType, SaveType.Update);

            bookList.First().Author = "测试作者";

            bookList.Save(directory, fileName, fileType, SaveType.Update);
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lacrima.Framework.Infrastructure;

namespace Lacrima.Framework.Test.Infrastruction.Adapter
{
    [TestClass]
    public class XmlSerializerTest
    {
        /// <summary>
        /// 示例参照：http://www.cnblogs.com/johnsmith/archive/2012/12/03/2799795.html
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {
            DateTime timer = DateTime.Now;
            Messenger msg = new Messenger();
            string path = @"..\Cache\Target.xml";

            MyXmlSerializer.SaveToXml<DateTime>(path, timer, "DateTime");

            Signature target = new Signature();

            MyXmlSerializer.SaveToXml<Signature>(path, target, "Checker");

        }

        [TestMethod]
        public void TestMethod2()
        {
            string path = @"..\Cache\Target.xml";
            Signature target = MyXmlSerializer.LoadFromXml<Signature>(path);
        }
    }
}

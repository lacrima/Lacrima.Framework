﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lacrima.Framework.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Threading;

namespace Lacrima.Framework.Test.Infrastructure.Utils
{
    [TestClass]
    public class CommonUtilsTest
    {
        /// <summary>
        /// 传递到 ref 形参的实参必须先经过初始化，然后才能传递。
        /// 这与 out 形参不同，在传递之前，不需要显式初始化该形参的实参。
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {
            int a = 10;
            int b = 100;
            CommonUtils.Swap(ref a, ref b);
        }

        [TestMethod]
        public void TestMethod2()
        {
            double a =  1500.455;
            double b = 1501;
            var target = Math.Ceiling(a);

            bool result = target.Equals(b);
            Assert.AreEqual(true,result);

            int index = (int)Math.Floor(a / 600);

            int key = (int)Math.Ceiling(a / 600) * 600;
            var dict = this.CreateFlightLevelContainer();

            dict[key] += 1;
        }


        [TestMethod]
        public void TestMethod3()
        {
            ServiceMessage serviceMessage1 = new ServiceMessage().Pad();
            ServiceMessage serviceMessage2 = new ServiceMessage().Pad();
            
            // 仅比较两个对象成员变量值是否相等
            Assert.IsTrue(serviceMessage1.IsValueEquals(serviceMessage2));
            // 按照系统规则对两个对象进行比较
            Assert.IsFalse(serviceMessage1.EqualsTo(serviceMessage2, true));
            Assert.IsTrue(serviceMessage1.EqualsTo(serviceMessage2, false));

            serviceMessage1.MessageException = null;
            Assert.IsFalse(serviceMessage1.EqualsTo(serviceMessage2, false));
        }

        private IDictionary<int, int> CreateFlightLevelContainer()
        {
            IDictionary<int, int> flightLevelDictionary = new Dictionary<int, int>();
            for (int i = 600; i < 65535; i += 600)
            {
                flightLevelDictionary.Add(i, 0);
            }
            return flightLevelDictionary;
        }

        [TestMethod]
        public void LinqEqualTest()
        {
            List<string> list1 = new List<string>();

            List<string> list2 = new List<string>();

            for (int i = 0; i < 10; ++i)
            {
                list1.Add(DateTime.Now.ToString());
                list2.Add(DateTime.Now.ToString());
            }

            var except = list1.Except(list2).ToList();
            var intersect = list1.Intersect(list2).ToList();

            bool isListEqual = list2.All(o => list1.Contains(o)) && list1.All(o => list2.Contains(o)) && list1.Count == list2.Count;


            Assert.IsTrue(isListEqual);

            list1.Clear();
            list2.Clear();

            list1.Add("1");
            list1.Add("1");
            list1.Add("3");

            list2.Add("1");
            list2.Add("2");
            list2.Add("3");

            isListEqual = list2.All(o => list1.Contains(o)) && list1.All(o => list2.Contains(o)) && list1.Count == list2.Count;
            Assert.IsFalse(isListEqual);
        }


        public void Print()
        {
            Debug.WriteLine("Starting...");
            for (int i = 1; i < 10; ++i)
            {
                Debug.WriteLine(i);
            }
        }

        [TestMethod]
        public void TestMethod4()
        {
            Thread t = new Thread(Print);

            t.Start();
            //Thread.Sleep(2000);
            //t.Join();.3
            t.Abort();
            Print();

            Debug.WriteLine(t.ThreadState);
        }
    }
}

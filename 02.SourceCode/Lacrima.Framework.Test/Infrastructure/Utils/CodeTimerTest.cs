﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lacrima.Framework.Infrastructure;
using System.Threading;
using System.Text;

namespace Lacrima.Framework.Test.Infrastructure.Utils
{
    [TestClass]
    public class CodeTimerTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            CodeTimer.Time("Thread Sleep", 1, () => { Thread.Sleep(3000); });
            CodeTimer.Time("Empty Method", 10000000, () => { });

            int iteration = 100 * 1000;

            string s = "";
            CodeTimer.Time("String Concat", iteration, () => { s += "a"; });

            StringBuilder sb = new StringBuilder();
            CodeTimer.Time("StringBuilder", iteration, () => { sb.Append("a"); });
        }
    }
}

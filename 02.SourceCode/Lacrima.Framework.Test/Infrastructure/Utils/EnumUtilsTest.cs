﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lacrima.Framework.Infrastructure;
using System.Linq;

namespace Lacrima.Framework.Test.Infrastruction.Utils
{
    [TestClass]
    public class EnumUtilsTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            int defaultAccessMode = 1;
            bool flag = EnumUtils.ContainsValue<ResponseState>(defaultAccessMode);
            Assert.AreEqual(true, flag);

            defaultAccessMode = int.MinValue;
            flag = EnumUtils.ContainsValue<ResponseState>(defaultAccessMode);
            Assert.AreEqual(false, flag);
        }

        [TestMethod]
        public void EnumFlagsTest()
        {
            // 航路类型
            int value = (int)AirRouteType.常规航线;
            // 对外开放类型
            int open = (int)AccessType.未对外开放;
            // 混合类型
            int hybrid = value | open;

            // 取航路类型
            int test = hybrid % (int)AirRouteType.Upper;

            Assert.AreEqual(test,value);

            // 取对外开放类型
            test = hybrid / (int)AirRouteType.Upper * (int)AirRouteType.Upper;

            Assert.AreEqual(test, open);
        }
    }
}

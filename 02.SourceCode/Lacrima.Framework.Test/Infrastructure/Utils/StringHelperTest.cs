﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lacrima.Framework.Infrastructure;
using System.Linq;
using Newtonsoft.Json;

namespace Lacrima.Framework.Test.Infrastruction.Utils
{
    [TestClass]
    public class StringHelperTest
    {
        // 新增注释信息
        [TestMethod]
        public void TestMethod1()
        {
            string target = "12312WHswh123whosyourdaddy";
            var test1 = StringHelper.alphabetCharSet;
            string test2 = target.TrimAlphabet();
        }

         // 转义字符替换测试
        [TestMethod]
        public void TestMethod2()
        {
            string target = @" &#x1;QU SZXUOZH
.BJSXCXA 151602
&#x2;DFD
FI ZH9428/AN B-6568
DT BJS TYN 151602 D45A
-  A12/A32012,1,2/6568'071516160102         001952530414742756
                0891208918          09770977        00
                                 11      0                  00500050
0
                    0245202452          0114401144
    
    

&#x3;";
            string result = target.Replace("'","\\'");
            bool flag = result.Contains("\'");
            Assert.AreEqual(true, flag);
        }
       
        // 字符串翻转
        [TestMethod]
        public void TestMethod3()
        {
            string target = "Hello World!";
            var result = target.Reverse();
        }

        // 字符串分割
        [TestMethod]
        public void TestMethod4()
        {
            string target = "Hello World!";
            var result = target.SplitByString("l");
            var result1 = target.SplitByString("ll");
            string temp = target.GetMD5HashCode();
        }

        // 字符掩码测试
        [TestMethod]
        public void TestMethod5()
        {
            string phone = "13812345678";
            string result = phone.Mask(-3,4);
            Assert.AreEqual("138****5678",result);
        }

        // 字符编码测试
        [TestMethod]
        public void TestMethod6()
        {
            //string phone = "手机号码：13812345678";
            //string result = phone.GetUrlEncode();
            //string recovery = result.GetUrlDecode();
            //Assert.AreEqual(phone, recovery);

            string origin = "timestamp={0}&soufunId={1}";
            var dictionary = origin.GetParamDictionary();

            // 示例参照：http://www.cnblogs.com/ldp615/archive/2009/08/12/1544688.html
            int[] nums = new int[] { 10, 20, 30, 40, 50 };
            int sum1 = nums.Where((n, i) => i % 2 == 0).Sum();//10 + 30 + 50
        }

        // 字符掩码测试
        [TestMethod]
        public void TestMethod7()
        {
            string phone = "abc#def#g#h#";
            int[] result = { 3, 7, 9, 11 };
            int[] array = phone.IndexArrayOf("#");
            Assert.AreEqual(result.Length, array.Length);

            for (int i = 0; i < result.Length;++i )
            {
                Assert.IsTrue(result[i] == array[i]);
            }

            phone = "abced";
            array = phone.IndexArrayOf("#");
            Assert.IsTrue(array == null);

            int index = phone.IndexOf("x");
            int index1 = phone.IndexOf("y");
            int r = Math.Max(index,index1);
        }

        //  JsonTest
        [TestMethod]
        public void JsonTest()
        {
            Messenger msg = new Messenger();
            var json = JsonConvert.SerializeObject(msg);
        }

        [TestMethod]
        public void DBCTEST()
        {
            string strSBC = "中华１２５９ｔｅｓｔＮＥＳＷ";
            string result = StringHelper.ToDBC(strSBC);
            Console.WriteLine(result);
            Assert.AreEqual(result, "中华1259testNESW");
        }
    }
}

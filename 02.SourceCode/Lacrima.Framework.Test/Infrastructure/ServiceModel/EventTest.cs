﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lacrima.Framework.Infrastructure;
using System.Collections.Generic;

namespace Lacrima.Framework.Test.Infrastructure
{
    [TestClass]
    public class EventTest
    {
        protected Heater heater { get; set; }
        protected Alarm alarm { get; set; }

        public EventTest()
        {
            heater = new Heater();
            alarm = new Alarm();
        }

        [TestMethod]
        public void TestMethod1()
        {
            heater.BoilEvent += alarm.MakeAlert;
            //heater.BoilEvent += (new Alarm()).MakeAlert;
            //heater.BoilEvent += Display.ShowMsg;
            heater.BoilWater();
        }

        [TestMethod]
        public void TestMethod2()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            // 此时会抛出异常
            string temp = dict["ss"];

            HashSet<string> hash = new HashSet<string>();
            hash.Contains("ss");
        }
    }
}

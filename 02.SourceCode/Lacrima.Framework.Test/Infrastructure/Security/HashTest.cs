﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lacrima.Framework.Infrastructure;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Lacrima.Framework.Test.Infrastruction.Security
{
    [TestClass]
    public class HashTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            string target = "你好";
            string result = MD5Provider.GetHashCode(target);

            string path = @"..\Cache\Target.xml";
            result = MD5Provider.GetFileHashCode(path);
        }

        [TestMethod]
        public void TestMethod2()
        {
            double value = 4.12d;
            string str = value.ToString("f1").PadLeft(4,'0');

            value = Math.Round(4.4d, MidpointRounding.AwayFromZero);

            value = Math.Round(4.45d, 1);

            value = Math.Round(4.45d, 1, MidpointRounding.AwayFromZero);
        }
    }
}

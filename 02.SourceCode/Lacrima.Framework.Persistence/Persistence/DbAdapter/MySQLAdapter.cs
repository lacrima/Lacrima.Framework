﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Persistence
{
    public sealed class MySQLAdapter : DbAdapter
    {
        public override IDbConnection GetDbConnection(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentNullException("数据库连接字符串为空，请检查数据库配置信息");
            }
            else
            {
                return new MySqlConnection(connectionString);
            }
        }

        public override DbDataAdapter GetDbDataAdapter(IDbCommand dbCommand)
        {
            return new MySqlDataAdapter(dbCommand as MySqlCommand);
        }
    }
}

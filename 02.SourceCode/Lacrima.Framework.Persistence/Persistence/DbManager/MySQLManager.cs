﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Lacrima.Framework.Persistence
{
    public sealed class MySQLManager : DbManager
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public MySQLManager(string connectionString)
            : base(connectionString)
        {
            this.dbAdapter = new MySQLAdapter();
        }

        public MySQLManager(Enum entity)
            : base(entity)
        {
            this.dbAdapter = new MySQLAdapter();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Persistence
{
    public sealed class SQLServerManager : DbManager 
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public SQLServerManager(string connectionString)
            : base(connectionString)
        {
            this.dbAdapter = new SQLServerAdapter();
        }

        public SQLServerManager(Enum entity)
            : base(entity)
        {
            this.dbAdapter = new SQLServerAdapter();
        }

        /// <summary>
        /// 执行数据库非查询方法
        /// </summary>
        /// <param name="strQuerySql">The string query SQL.</param>
        /// <param name="table">The table.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public override IDataReader ExecuteQuery(string strQuerySql)
        {
            try
            {
                this.dbCommand.CommandText = strQuerySql;
                return this.dbCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Persistence
{
    /// <summary>
    /// MySQL数据库的仓储类模型，该类为密封类，不能被继承
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class SQLServerRepository<T> : Repository<T> where T : class, new()
    {
        public SQLServerRepository(SQLServerManager manager)
            : base(manager)
        {
        }

        /// <summary>
        /// 默认查询所有数据的方法
        /// </summary>
        /// <returns>IEnumerable{`0}.</returns>
        public override DataTable QueryDefault()
        {
            DataTable table = new DataTable();
            string sql = string.Format("SELECT * FROM {0}", typeof(T).Name);
            IDataReader reader = this.dbManager.ExecuteQuery(sql);
            DataColumn col;
            DataRow row;
            int i;

            for (i = 0; i < reader.FieldCount; i++)
            {
                col = new DataColumn();
                col.ColumnName = reader.GetName(i);
                col.DataType = reader.GetFieldType(i);
                table.Columns.Add(col);
            }

            while (reader.Read())
            {
                row = table.NewRow();
                for (i = 0; i < reader.FieldCount; i++)
                {
                    row[i] = reader[i];
                }
                table.Rows.Add(row);
            }
            return table;
        }
    }
}

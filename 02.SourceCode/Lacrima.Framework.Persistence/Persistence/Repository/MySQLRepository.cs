﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Persistence
{
    /// <summary>
    /// MySQL数据库的仓储类模型，该类为密封类，不能被继承
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class MySQLRepository<T> : Repository<T> where T : class, new()
    {
        public MySQLRepository(MySQLManager manager)
            : base(manager)
        {
        }
    }
}

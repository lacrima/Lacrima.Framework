﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Persistence
{
    public static class MySQLMapper
    {
        private static IDictionary<string, string> _typeMapper { get; set; }

        public static IDictionary<string, string> typeMapper
        {
            get
            {
                if(_typeMapper == null)
                {
                    _typeMapper = new Dictionary<string, string>();
                    _typeMapper.Add("Int64","bigint");
                    _typeMapper.Add("Byte[]", "timestamp");
                    _typeMapper.Add("Boolean", "bit");
                    _typeMapper.Add("String", "varchar(100)");
                    _typeMapper.Add("Decimal", "decimal");
                    _typeMapper.Add("Single", "float");
                    _typeMapper.Add("Int16", "tinyint");
                    _typeMapper.Add("DateTime", "datetime");
                    _typeMapper.Add("Int32", "int");
                    _typeMapper.Add("Double", "double");
                }
                return _typeMapper;
            }
        }

        public static string GetMapType(string entityType)
        {
            if (typeMapper.ContainsKey(entityType))
            {
                return typeMapper[entityType];
            }
            else
            {
                return string.Empty;
            }
        }

    }
}

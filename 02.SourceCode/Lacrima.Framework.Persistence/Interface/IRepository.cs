﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Persistence
{
    public interface IRepository<T> where T : class, new()
    {
        IDbManager dbManager { get; set; }

        IEnumerable<T> QueryAll();

        DataTable QueryDefault();

        IEnumerable<T> Query(string querySQL);

        int Execute(string nonQuerySQL);

        TResult QueryScalar<TResult>(string scalarSQL);

        void ExecuteDelegate(Delegate method);
    }
}

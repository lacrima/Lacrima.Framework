﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Persistence
{
    public interface IDbManager : IDisposable
    {
        // <summary>
        /// 打开链接。
        /// </summary>
        /// <returns></returns>
        bool Open();

        /// <summary>
        /// 打开链接,使用默认事务。与回滚事务或提交事务配合使用。 
        /// </summary>
        /// <returns></returns>
        bool OpenForTransaction();

        /// <summary>
        /// 执行非查询操作。
        /// </summary>
        /// <param name="strNonQuerySql"></param>
        /// <returns></returns>
        int ExecuteNonQuery(string strNonQuerySql);

        /// <summary>
        /// 执行查询操作。
        /// </summary>
        /// <param name="strQuerySql"></param>
        /// <returns></returns>
        IDataReader ExecuteQuery(string strQuerySql);

        /// <summary>
        /// 执行查询操作。
        /// </summary>
        /// <param name="strQuerySql"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        bool ExecuteQuery(string strQuerySql, out DataTable dt);

        /// <summary>
        /// 回滚事务。
        /// </summary>
        /// <returns></returns>
        bool Rollback();

        /// <summary>
        /// 提交事务。
        /// </summary>
        /// <returns></returns>
        bool Commit();

        /// <summary>
        /// 关闭连接。
        /// </summary>
        /// <returns></returns>
        void Close();
    }
}

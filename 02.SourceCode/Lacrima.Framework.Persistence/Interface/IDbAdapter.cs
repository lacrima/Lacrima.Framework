﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Persistence
{
    public interface IDbAdapter
    {
        IDbAdapter GetInstance();
        IDbConnection GetDbConnection(string connectionString);
        IDbCommand GetDbCommand(IDbConnection dbConnection);
        IDbTransaction GetDbTransaction(IDbConnection dbConnection);
        DbDataAdapter GetDbDataAdapter(IDbCommand dbCommand);
        void Dispose(IDbConnection dbConnection);
        void Dispose(IDbCommand dbCommand);
        void Dispose(IDbTransaction dbTransaction);
    }
}

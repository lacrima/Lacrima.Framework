﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Persistence
{
    public interface IEntityMapper
    {
        string EntityToTable<TEntity>(TEntity entity);
    }
}

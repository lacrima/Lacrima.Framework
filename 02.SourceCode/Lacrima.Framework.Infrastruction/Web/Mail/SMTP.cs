﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
    /// <summary>
    /// http://www.cnblogs.com/xililiu/archive/2012/03/24/2415657.html
    /// </summary>
    public class SMTP
    {
        /// Send email without attachments
        /// </summary>
        /// <param name="ToMail">收件人邮箱地址</param>
        /// <param name="FromMail">发件人邮箱地址</param>
        /// <param name="Cc">抄送</param>
        /// <param name="Bcc">密送</param>
        /// <param name="Body">邮件正文</param>
        /// <param name="Subject">邮件标题</param>
        /// <returns></returns>
        public string SendMail(string ToMail, string FromMail, string Cc, string Bcc, string Body, string Subject)
        {
            SmtpClient client = new SmtpClient();
            MailMessage message = new MailMessage
            {
                From = new MailAddress(FromMail)
            };
            message.To.Add(ToMail);
            if (Cc != "")
            {
                message.CC.Add(Cc);
            }
            message.Body = Body;
            message.Subject = Subject;
            message.IsBodyHtml = true;
            client.UseDefaultCredentials = true;
            message.Priority = MailPriority.High;
            client.Host = "192.168.246.20";//此处应该改为上面设置的服务器IP地址
            client.Port = 0x19;
            try
            {
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(message);
                message.Dispose();
                return "1";
            }
            catch (Exception exception)
            {
                return ("0" + exception);
            }
        }

        ///  Send email without attachments
        /// </summary>
        /// <param name="ToMail">收件人邮箱地址</param>
        /// <param name="FromMail">发件人邮箱地址</param>
        /// <param name="Cc">抄送</param>
        /// <param name="Bcc">密送</param>
        /// <param name="Body">邮件正文</param>
        /// <param name="Subject">邮件标题</param>
        /// <param name="Attachments">附件列表</param>
        /// <returns></returns>
        public string SendMailWithAttachment(string ToMail, string FromMail, string Cc, string Bcc, string Body, string Subject, string[] Attachments)
        {
            SmtpClient client = new SmtpClient();
            MailMessage message = new MailMessage
            {
                From = new MailAddress(FromMail)
            };
            message.To.Add(ToMail);
            if (Cc != "")
            {
                message.CC.Add(Cc);
            }

            message.Body = Body;
            message.Subject = Subject;
            message.IsBodyHtml = true;
            message.Priority = MailPriority.High;
            if (Attachments.Length > 0)
            {
                for (int i = 0; i < Attachments.Length; i++)
                {
                    if (Attachments[i].ToString() != "")
                    {
                        Attachment item = new Attachment(Attachments[i].ToString());
                        message.Attachments.Add(item);
                    }
                }
            }
            client.Host = "127.0.0.1";//此处应该改为上面设置的服务器IP地址
            client.Port = 0x19;
            try
            {
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(message);
                message.Dispose();
                return "1";
            }
            catch (Exception exception)
            {
                return ("0" + exception);
            }
        }
    }
}

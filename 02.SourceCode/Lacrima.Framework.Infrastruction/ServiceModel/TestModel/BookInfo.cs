﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
    /// <summary>  
    /// 图书信息实体类  
    /// </summary>  
    public class BookInfo : IEntityModel
    {
        public BookInfo()
        {
            this.Identify = Generator.UID;
        }

        public string Identify { get; set; }
        public int BookId { set; get; }             //图书ID  
        public string Title { set; get; }           //图书名称  
        public string Category { set; get; }        //图书分类  
        public string Author { set; get; }          //图书作者  
        public DateTime PublishDate { set; get; }   //出版时间  
        public Double Price { set; get; }           //销售价格  
    }  
}

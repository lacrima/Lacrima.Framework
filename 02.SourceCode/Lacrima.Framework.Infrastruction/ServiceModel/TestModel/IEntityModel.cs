﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
    /// <summary>
    /// 数据实体类基础接口
    /// </summary>
    public interface IEntityModel
    {
        [Key]
        /// <summary>
        /// 唯一标示
        /// <summary>
        string Identify { get; set; }
    }
}

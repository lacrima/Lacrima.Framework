﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
   public class Heater
   {
       private int temperature;

       public delegate void BoilHandler(int param);

       public event BoilHandler BoilEvent;

       public void BoilWater()
       {
           for (int i = 0; i < 100; i++)
           {
               this.temperature = i;
               if (this.temperature > 95)
               {
                   if (this.BoilEvent != null)
                   {
                       BoilEvent(this.temperature);
                   }
               }
           }
       }
   }

    public class Alarm
    {
        public void MakeAlert(int param)
        {
            Debug.WriteLine("Alarm 滴滴滴，水已经{0}度了", param);
        }
    }

    public class Display
    {
        public static void ShowMsg(int param)
        {
            Debug.WriteLine("Alarm 滴滴滴，水已经{0}度了", param);
        }
    }
}

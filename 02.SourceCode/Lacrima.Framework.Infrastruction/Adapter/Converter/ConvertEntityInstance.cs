﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
    public class ConvertEntityInstance
    {
        public int intColumn { get; set; }
        public bool boolColumn { get; set; }
        public short shortColumn { get; set; }
        public long longColumn { get; set; }
        public byte byteColumn { get; set; }
        public byte[] bytesColumn { get; set; }
        public string stringColumn { get; set; }
        public DateTime DateTimeColumn { get; set; }
        public float floatColumn { get; set; }
        public double doubleColumn { get; set; }
        public decimal decimalColumn { get; set; }
    }
}

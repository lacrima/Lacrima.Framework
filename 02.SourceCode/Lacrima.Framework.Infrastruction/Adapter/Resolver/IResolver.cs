﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
    public interface IResolver
    {
        TSourceMember ResolveValue<TSource, TSourceMember>(TSource source, Expression<Func<TSource, TSourceMember>> expression) where TSource : class;

        bool ResolveEquals<TSource, TDestination>(TSource source, TDestination destination);
    }
}

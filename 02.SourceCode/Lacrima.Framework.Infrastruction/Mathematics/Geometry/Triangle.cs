﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
    /// <summary>
    /// 三角形类
    /// </summary>
    public class Triangle
    {
        public double angleA { get; set; }
        public double angleB { get; set; }
        public double angleC { get; set; }

        public double sideA { get; set; }
        public double sideB { get; set; }
        public double sideC { get; set; }
    }
}

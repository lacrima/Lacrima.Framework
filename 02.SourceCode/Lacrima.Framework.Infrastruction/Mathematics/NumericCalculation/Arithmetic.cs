﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
    /// <summary>
    /// 算术功能类
    /// </summary>
    public class Arithmetic
    {
        /// <summary>
        /// 获取向左偏移两位的数值
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>System.Double.</returns>
        public static double GetNumberLeftOffsetTwoDigtal(double source)
        {
            return GetNumberLeftOffset(source, 2);
        }

        /// <summary>
        /// 获取数值向左偏移两位的结果
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="offsetSize">偏移位数</param>
        /// <returns>计算结果</returns>
        public static double GetNumberLeftOffset(double source, int offsetSize)
        {
            if (!double.IsNaN(source)
                && !double.IsNegativeInfinity(source)
                && !double.IsPositiveInfinity(source))
            {
                // 这里没有加括号是因为C#的计算顺序总是从左向右的
                return (source * Math.Pow(10, offsetSize) % Math.Pow(10, offsetSize));
            }
            return double.NaN;
        }

        /// <summary>
        /// 判断目标参数是否为负数
        /// </summary>
        public static bool IsNegative(int source)
        {
            return source < 0;
        }

        /// <summary>
        /// 判断目标参数是否为负数
        /// </summary>
        public static bool IsNegative(float source)
        {
            return !float.IsNaN(source) && source < (float.Epsilon * -1);
        }

        /// <summary>
        /// 判断目标参数是否为负数
        /// </summary>
        public static bool IsNegative(double source)
        {
            return !double.IsNaN(source) && source < (double.Epsilon * -1);
        }

        /// <summary>
        /// 判断目标参数是否为负数
        /// </summary>
        public static bool IsNegative(decimal source)
        {
            return source < decimal.Zero;
        }

        /// <summary>
        /// 判断目标参数是否为正数
        /// </summary>
        public static bool IsPositive(int source)
        {
            return source > 0;
        }

        /// <summary>
        /// 判断目标参数是否为正数
        /// </summary>
        public static bool IsPositive(float source)
        {
            return !float.IsNaN(source) && source > float.Epsilon;
        }

        /// <summary>
        /// 判断目标参数是否为正数
        /// </summary>
        public static bool IsPositive(double source)
        {
            return !double.IsNaN(source) && source > double.Epsilon;
        }

        /// <summary>
        /// 判断目标参数是否为正数
        /// </summary>
        public static bool IsPositive(decimal source)
        {
            return source > decimal.Zero;
        }
    }
}

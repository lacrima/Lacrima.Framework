﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
    public enum BasicTypesEnum
    {
        Int64 = 1,
        Byte = 2,
        Boolean = 3,
        String = 4,
        Decimal = 5,
        Single = 6,
        Int16 = 7,
        DateTime = 8,
        Int32 = 9,
        Double = 10,
        UInt32 = 11,
        UInt16 = 12,
        UInt64 = 13
    }
}

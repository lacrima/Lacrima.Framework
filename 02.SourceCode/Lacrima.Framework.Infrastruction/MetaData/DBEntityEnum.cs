﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
    /// <summary>
    /// 数据库实体枚举
    /// </summary>
    public enum DBEntityEnum
    {
        WORLD = 1,
        MySQL = 2,
        OpenTestDB = 3,
    }

    [Flags]
    public enum AirRouteType
    {
        None = 0,
        常规航路 = 1,
        常规航线 = 2,
        进离场航线 = 4,
        区域导航航路 = 8,
        临时航线 = 16,
        Upper = 32,
    }

    [Flags]
    public enum AccessType
    {
        None = 0,
        对外开放 = 32,
        未对外开放 = 64,
        Upper = 128,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
    /// <summary>
    /// 数据库访问类型
    /// </summary>
    public enum DataProviderEnum
    {
        Oracle = 0,
        SQLServer = 1,
        MySQL = 2,
        Access = 3
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
    public static partial class DataHelper
    {
        /// <summary>
        /// 将DataRow行转换成EntityModel
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static TEntity ToEntity<TEntity>(this DataRow row) where TEntity : new()
        {
            TEntity entity = new TEntity();
            Type type = typeof(TEntity);
            PropertyInfo[] properties = type.GetProperties();
            foreach (var property in properties)
            {
                if (property != null && row.Table.Columns.Contains(property.Name))
                {
                        //根据ColumnName，将dr中的相对字段赋值给Entity属性，强制类型转换
                        property.SetValue(entity, Convert.ChangeType(row[property.Name], property.PropertyType), null);
                }
            }
            return entity;
        }

        /// <summary>
        /// 将DataTable行转换成List<EntityModel>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt">The dt.</param>
        /// <returns>List{``0}.</returns>
        public static List<TEntity> ToEntityList<TEntity>(this DataTable table) where TEntity : new()
        {
            List<TEntity> list = new List<TEntity>(table.Rows.Count);
            foreach (DataRow dr in table.Rows)
            {
                list.Add(dr.ToEntity<TEntity>());
            }
            return list;
        }

        /// <summary>
        /// 实体类转换成DataTable
        /// </summary>
        /// <param name="modelList">实体类列表</param>
        /// <returns></returns>
        public static DataTable FillDataTable<T>(this List<T> modelList) where T : class
        {
            if (modelList == null || modelList.Count == 0)
            {
                return null;
            }
            DataTable dt = CreateDataTable<T>();

            foreach (T model in modelList)
            {
                DataRow dataRow = dt.NewRow();
                foreach (PropertyInfo propertyInfo in typeof(T).GetProperties())
                {
                    dataRow[propertyInfo.Name] = propertyInfo.GetValue(model, null);
                }
                dt.Rows.Add(dataRow);
            }
            return dt;
        }

        /// <summary>
        /// 根据实体类得到表结构
        /// </summary>
        /// <param name="model">实体类</param>
        /// <returns></returns>
        public static DataTable CreateDataTable<T>() where T : class
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            foreach (PropertyInfo propertyInfo in typeof(T).GetProperties())
            {
                dataTable.Columns.Add(new DataColumn(propertyInfo.Name, propertyInfo.PropertyType));
            }
            return dataTable;
        }

        /// <summary>
        /// 根据实体类得到DataTable
        /// </summary>
        /// <param name="model">实体类</param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this T model) where T : class
        {
            DataTable datatable = CreateDataTable<T>();
            DataRow dataRow = datatable.NewRow();
            foreach (PropertyInfo propertyInfo in typeof(T).GetProperties())
            {
                dataRow[propertyInfo.Name] = propertyInfo.GetValue(model, null);
            }
            datatable.Rows.Add(dataRow);
            return datatable;
        }
    }
}

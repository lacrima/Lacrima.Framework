﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
    public static partial class StringHelper
    {
        /// <summary>
        /// 数字字符集
        /// </summary>
        public static readonly char[] numberCharSet = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

        /// <summary>
        /// 小写字母字符集
        /// </summary>
        public static readonly char[] alphabetLowerCaseCharSet = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

        /// <summary>
        /// 大写字母字符集
        /// </summary>
        public static readonly char[] alphabetUpperCaseCharSet = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

        /// <summary>
        /// 字母表字符集（包括大写字母和小写字母）
        /// </summary>
        public static readonly char[] alphabetCharSet = alphabetUpperCaseCharSet.Concat(alphabetLowerCaseCharSet).ToArray();

        /// <summary>
        /// 在目标字符串中去除掉字符集中所包含的所有字符
        /// </summary>
        /// <param name="str">目标字符串</param>
        /// <param name="trimCharSet">要去除的字符集</param>
        /// <returns>结果字符串</returns>
        public static string TrimCharSet(this string str, params char[] trimCharSet)
        {
            if(!string.IsNullOrEmpty(str))
            {
                return string.Join(string.Empty, str.ToCharArray().Where(ch => !trimCharSet.Contains(ch)));
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 去除字符串中的所有数字
        /// </summary>
        /// <param name="str">目标字符串</param>
        /// <returns>结果字符串</returns>
        public static string TrimDigtal(this string str)
        {
            return TrimCharSet(str, numberCharSet);
        }

        /// <summary>
        /// 去除字符串中的小写字母
        /// </summary>
        /// <param name="str">目标字符串</param>
        /// <returns>结果字符串</returns>
        public static string TrimLower(this string str)
        {
            return TrimCharSet(str, alphabetLowerCaseCharSet);
        }

        /// <summary>
        /// 去除字符串中的大写字母
        /// </summary>
        /// <param name="str">目标字符串</param>
        /// <returns>结果字符串</returns>
        public static string TrimUpper(this string str)
        {
            return TrimCharSet(str, alphabetUpperCaseCharSet);
        }

        /// <summary>
        /// 去除字符串中的大小写字母
        /// </summary>
        /// <param name="str">目标字符串</param>
        /// <returns>结果字符串</returns>
        public static string TrimAlphabet(this string str)
        {
            return TrimCharSet(str, alphabetCharSet);
        }

        /// <summary>
        /// 在目标字符串中去除掉字符集中所包含的所有字符
        /// </summary>
        /// <param name="str">目标字符串</param>
        /// <param name="predicate">匹配表达式</param>
        /// <returns>结果字符串</returns>
        public static string TrimByExpression(this string str, Func<char, bool> predicate)
        {
            return string.Join(string.Empty, str.ToCharArray().Where(predicate));
        }

        /// <summary>
        /// 对字符串数组进行Trim
        /// </summary>
        /// <param name="source">目标字符串</param>
        /// <param name="targetChar">要Trim的字符</param>
        /// <returns>结果字符串数组</returns>
        public static IEnumerable<string> ArrayTrim(this IEnumerable<string> source, char targetChar)
        {
            return source.Select(o => o.Trim(targetChar));
        }

        /// <summary>
        /// 截取起始符号和终止符号之间的字符串
        /// </summary>
        /// <param name="source">字符串源</param>
        /// <param name="startChar">起始字符</param>
        /// <param name="endChar">结束字符</param>
        /// <param name="startCharWithIn">包含起始字符</param>
        /// <param name="endCharWithIn">包含结束字符</param>
        /// <param name="startCharSeq">起始字符的序号，即从第几个起始字符开始截取</param>
        /// <param name="endCharSeq">结束字符的序号，即从第几个结束字符开始截取</param>
        /// <returns>截取的字符串结果</returns>
        public static string SubStringBetween(this string source, char startChar, char endChar, bool startCharWithIn = true, bool endCharWithIn = true, int startCharSeq = 1, int endCharSeq = 1)
        {
            if (!string.IsNullOrEmpty(source))
            {
                char[] charSet = source.ToCharArray();
                int startIndex = -1, endIndex = -1;
                for (int index = 0, startSeq = 0, endSeq = 0; index < charSet.Length; ++index)
                {
                    if (startChar.Equals(charSet[index]) && (++startSeq) == startCharSeq)
                    {
                        startIndex = index;
                    }

                    if (startIndex > -1 && startIndex <= index && endChar.Equals(charSet[index]) && (++endSeq) == endCharSeq)
                    {
                        endIndex = index;
                    }
                }

                if (startIndex > -1 && endIndex > -1 && startIndex < charSet.Length && endIndex < charSet.Length && startIndex <= endIndex)
                {
                    string result = source.Substring(startIndex, endIndex - startIndex + 1);
                    if (!startCharWithIn)
                    {
                        result = result.TrimStart(startChar);
                    }

                    if (!endCharWithIn)
                    {
                        result = result.TrimEnd(endChar);
                    }
                    return result;
                }
                else
                {
                    return string.Empty;
                }
            }
            return source;
        }
    }
}

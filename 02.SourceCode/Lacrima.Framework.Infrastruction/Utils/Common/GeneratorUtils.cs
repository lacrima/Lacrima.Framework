﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
    /// <summary>
    /// 生成器帮助类
    /// </summary>
    public static class GeneratorUtils
    {
        /// <summary>
        /// 随机数生成工具
        /// </summary>
        public static readonly Random Rand = new Random();

        /// <summary>
        /// 全局唯一值生成工具
        /// </summary>
        /// <value>全局唯一值</value>
        public static string UID
        {
            get
            {
                return Guid.NewGuid().ToString();
            }
        }

        /// <summary>
        /// 通过随机数生成随机布尔值
        /// </summary>
        /// <value>随机的布尔值</value>
        public static bool RandomBoolean
        {
            get
            {
                return (int)Math.Ceiling(Math.Sin(Rand.Next())) == 1;
            }
        }

        /// <summary>
        /// 获取枚举类型的随机值
        /// </summary>
        /// <param name="propertyType">枚举类型</param>
        /// <returns>该枚举类型的随机枚举值</returns>
        public static object GetRandomEnumValue(Type propertyType)
        {
            // 设置随机枚举值
            if (propertyType.IsEnum)
            {
                Array enumValues = propertyType.GetEnumValues();
                IEnumerator enumerator = enumValues.GetEnumerator();
                int randIndex = Rand.Next(0, enumValues.Length);
                for (int index = 0; index < enumValues.Length && enumerator.MoveNext(); ++index)
                {
                    if (index == randIndex)
                    {
                        return enumerator.Current;
                    }
                }
            }
            return null;
        }
    }
}

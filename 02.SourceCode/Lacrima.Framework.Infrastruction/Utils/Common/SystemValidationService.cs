﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Infrastructure
{
    public static class SystemValidationService
    {
        public static string SQLServerService = "MSSQLSERVER";
        public static string MySQLService = "MySQL";


        public static bool IsExistSqlServerService()
        {
            return IsExistService(SQLServerService);
        }

        public static bool IsExistMySQLService()
        {
            return IsExistService(MySQLService);
        }

        public static bool IsExistService(string serviceName)
        {
            ServiceController[] services = ServiceController.GetServices();
            ServiceController service = services.FirstOrDefault(o => o.DisplayName.ContainsSubString(serviceName, true));
            return service != null && service.Status == ServiceControllerStatus.Running;
        }

        public static bool ContainsSubString(this string source, string target, bool ignoreCase = true)
        {
            if (string.IsNullOrEmpty(source) || string.IsNullOrEmpty(target))
                return false;
            else if (ignoreCase)
            {
                int index = source.ToUpper().IndexOf(target.ToUpper());
                return index >= 0;
            }
            else
            {
                int index = source.IndexOf(target);
                return index >= 0;
            }
        }
    }
}

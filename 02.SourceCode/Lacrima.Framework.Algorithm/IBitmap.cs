﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacrima.Framework.Algorithm
{
    /// <summary>
    /// bitmap算法类接口
    /// </summary>
    public interface IBitmap
    {
        byte[] Entry { get; set; }

        void Add(int source);

        void Remove(int source);

        bool Contains(int source);

        void Clear();
    }
}
